from flask import jsonify, make_response
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import datetime
from social_network.models.socials.user import User
from flask import current_app


class Authentication:
    def authenticate_token(self,user,user_password,auth_password):
        user_password = user_password
        auth_password = auth_password
        if check_password_hash(user_password, auth_password):
            token = jwt.encode(
                {'public_id': user.public_id, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)},
                current_app.config['SECRET_KEY'])
            return jsonify({'token': token.decode('UTF-8')})
        return jsonify({'message': "unable to generate token !"})
