import json
import datetime
from http import HTTPStatus
from decimal import Decimal
from social_network import db
from flask_login import current_user
from social_network.models.socials.Post import Post
from social_network.models.socials.user import User
from social_network.models.socials.Comment import Comment
from flask import Flask, request, jsonify, make_response
import json

class UserPostService:
    def __init__(self):
        pass

    def default(self,obj):
        if isinstance(obj, Decimal):
            return str(obj)
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()
        raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)

    def get_posts(self,author_id):
        posts = Post.query.filter(Post.author_id == author_id).all()

        output = []

        for post in posts:
            user_post_data = {}
            user_comment=[]
            user_post_data['post_id'] = post.id
            user_post_data['post_content'] = post.content
            user_post_data['user_id'] = post.author_id
            user_post_data['like'] = post.like
            user_post_data['flag'] = post.flag
            user_post_data['share'] = post.share
            comments_data = Comment.query.filter_by(post_id=post.id).all()
            if comments_data:
                for comment in comments_data:
                    user_comment_data = {}
                    user_comment_data['comment_id'] = comment.id
                    user_comment_data['content'] = comment.content
                    user_comment_data['user_id'] = comment.author_id
                    user_comment_data['post_id'] = comment.post_id
                    user_comment_data['date_posted'] = comment.date_posted
                    user_comment.append(user_comment_data)
                user_post_data['comments'] = user_comment
            else:
                user_post_data['comments'] = []

            output.append(user_post_data)
        return json.dumps(output, default=self.default)

    def create_post(self, author_id ,post_data,current_user):
        # if not current_user.admin:
        #     return jsonify({'message':'only admin is allowed to perform'})
        try:
            new_post = Post(id=post_data['id'], content=post_data['content'], author_id=post_data['author_id'],
                            like=post_data['like'],share=post_data['share'],flag=post_data['flag'])
            db.session.add(new_post)
            db.session.commit()
            return jsonify({'message': "posts created!","status":HTTPStatus.CREATED})
        except Exception as e:
            return jsonify({"message":"unable to create post","status":HTTPStatus.CONFLICT})

    def get_post(self,author_id,post_id):
        posts = Post.query.filter(Post.id == post_id).all()
        output = []
        for post in posts:
            user_post_data = {}
            user_comment=[]
            user_post_data['post_id'] = post.id
            user_post_data['post_content'] = post.content
            user_post_data['like'] = post.like
            user_post_data['user_id'] = post.author_id
            user_post_data['flag'] = post.flag
            user_post_data['share'] = post.share
            comments_data = Comment.query.filter_by(post_id=post.id).all()
            if comments_data:
                for comment in comments_data:
                    user_comment_data = {}
                    user_comment_data['comment_id'] = comment.id
                    user_comment_data['content'] = comment.content
                    user_comment_data['user_id'] = comment.author_id
                    user_comment_data['post_id'] = comment.post_id
                    user_comment_data['date_posted'] = comment.date_posted
                    user_comment.append(user_comment_data)
                user_post_data['comments'] = user_comment
            else:
                user_post_data['comments'] = []

            output.append(user_post_data)
        return json.dumps(output, default=self.default)

    def update_post(self,author_id,post_id,post_data):
        posts_data = Post.query.filter_by(id =post_id).first()
        output = []
        try:
            # print(posts_data)
            posts_data.id = post_data['id']
            posts_data.content = post_data['content']
            posts_data.author_id = post_data['author_id']
            posts_data.like = post_data['like']
            posts_data.flag = post_data['flag']
            posts_data.share= post_data['share']
            db.session.add(posts_data)
            db.session.commit()
            return jsonify({'message': "posts updated!","status":HTTPStatus.OK})
        except Exception as e:
            return jsonify({"message":"unable to update post","status":HTTPStatus.CONFLICT})

    def delete_post(self, author_id, post_id):
        # if not current_user.admin:
        #     return jsonify({'message': 'only admin allowed to perform this function'})
        post = Post.query.filter_by(id=post_id).first()
        if not post:
            return jsonify({'message': "unable to delete post !", "status": HTTPStatus.CONFLICT})
        comment=Comment.query.filter(Comment.post_id==post_id).delete()
        db.session.delete(post)
        db.session.commit()
        return jsonify({'message': "posts deleted!","status":HTTPStatus.OK})

