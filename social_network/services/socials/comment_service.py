import json
import datetime
from http import HTTPStatus
from decimal import Decimal
from social_network import db
from flask_login import current_user
from social_network.models.socials.Post import Post
from social_network.models.socials.user import User
from social_network.models.socials.Post import Comment
from flask import Flask, request, jsonify, make_response
import json

class UserCommentService:
    def __init__(self):
        pass

    def default(self,obj):
        if isinstance(obj, Decimal):
            return str(obj)
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()
        raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)

    def get_comments(self,author_id,post_id):
        comments = Comment.query.filter(Comment.author_id == author_id,Comment.post_id==post_id).all()
        output = []

        for comment in comments:
            user_comment_data = {}
            user_comment_data['comment_id'] = comment.id
            user_comment_data['content'] = comment.content
            user_comment_data['user_id'] = comment.author_id
            user_comment_data['post_id'] = comment.post_id
            user_comment_data['date_posted'] = comment.date_posted

            output.append(user_comment_data)
        return json.dumps(output, default=self.default)

    def create_comment(self, author_id ,post_id,post_data,current_user):
        # if not current_user.admin:
        #     return jsonify({'message':'only admin is allowed to perform'})
        try:
            new_comment = Comment(id=post_data['id'], content=post_data['content'], author_id=post_data['author_id'],
                                  post_id=post_data['post_id'], date_posted=post_data['date_posted'], )
            db.session.add(new_comment)
            db.session.commit()
        except Exception as e:
            return jsonify({"message": "unable to create comment", "status": HTTPStatus.CONFLICT})

        return jsonify({'message': "comments created!"})

    def get_comment(self,author_id,post_id,comment_id):
        comments = Comment.query.filter(Comment.id == comment_id).all()
        output = []
        for comment in comments:
            user_comment_data = {}
            user_comment_data['id'] = comment.id
            user_comment_data['content'] = comment.content
            user_comment_data['user_id'] = comment.author_id
            user_comment_data['post_id'] = comment.post_id
            user_comment_data['date_posted'] = comment.date_posted

            output.append(user_comment_data)
        return json.dumps(output, default=self.default)

    def update_comment(self,author_id,post_id,comment_id,post_data):
        # import pdb
        # pdb.set_trace();
        comments_data = Comment.query.filter_by(id =comment_id).first()
        output = []
        try:
            comments_data.id = post_data['id']
            comments_data.content = post_data['content']
            comments_data.author_id = post_data['author_id']
            comments_data.post_id = post_data['post_id']
            comments_data.date_posted = post_data['date_posted']
            db.session.add(comments_data)
            db.session.commit()
            return jsonify({'message': "comment updated!", "status": HTTPStatus.OK})
        except Exception as e:
            return jsonify({"message": "unable to update comment", "status": HTTPStatus.CONFLICT})

    def delete_comment(self,author_id,post_id, comment_id):
        # if not current_user.admin:
        #     return jsonify({'message': 'only admin allowed to perform this function'})

        comment = Comment.query.filter_by(id=comment_id).first()
        if not comment:
            return jsonify({'message': "unable to delete comment !", "status": HTTPStatus.CONFLICT})
        db.session.delete(comment)
        db.session.commit()
        return jsonify({'message': "comments deleted!", "status": HTTPStatus.OK})








            