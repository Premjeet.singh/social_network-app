import json
import datetime
from http import HTTPStatus
from decimal import Decimal
from social_network import db
from flask_login import current_user
from social_network.models.socials.Post import Post
from social_network.models.socials.user import User
from social_network.models.connections.user_connection import UserConnectionMap
from social_network.models.likes_and_posts.likes import UserLikesAndShareMap
from social_network.models.socials.Comment import Comment
from flask import Flask, request, jsonify, make_response
import json


class UserPostLikesService:
    def __init__(self):
        pass

    def default(self,obj):
        if isinstance(obj, Decimal):
            return str(obj)
        raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)

    def user_posts_likes(self,post_data,author_id,post_id):
        posts = Post.query.filter(Post.id == post_id, Post.author_id == author_id).all()
        user_likes_data = UserLikesAndShareMap.query.filter(UserLikesAndShareMap.post_id == post_id, UserLikesAndShareMap.user_id == author_id).all()
        if user_likes_data:
            for user in user_likes_data:
                user.like=post_data['like']
                db.session.add(user)
                db.session.commit()
        else:
            new_likes = UserLikesAndShareMap(post_id=post_id, user_id=author_id,
                                         like=True, share=post_data['share'])
            db.session.add(new_likes)
            db.session.commit()

        if posts:
            for post in posts:
                post.like = post_data['like']
                db.session.add(post)
                db.session.commit()
            return jsonify({'message': "user like post!", "status": HTTPStatus.OK})
        elif post_data['flag'] == True:
            posts = Post.query.filter(Post.id == post_id).all()
            for post in posts:
                post.like = post_data['like']
                post.flag = True
                db.session.add(post)
                db.session.commit()
            return jsonify({'message': "user like post!", "status": HTTPStatus.OK})
        else:
            users = UserConnectionMap.query.filter(UserConnectionMap.user_id == author_id).all()
            user_connection_ids = [user.connection_id for user in users]
            posts = Post.query.filter(Post.id == post_id, Post.author_id.in_(user_connection_ids)).all()
            if posts:
                for post in posts:
                    post.like = post_data['like']
                    db.session.add(post)
                    db.session.commit()
                posts_data= Post.query.filter(Post.id == post_id).all()
                for post in posts_data:
                    post.like = post_data['like']
                    db.session.add(post)
                    db.session.commit()
                return jsonify({'message': "user like post!", "status": HTTPStatus.OK})
        return jsonify({'message': "user unable to like  post!", "status": HTTPStatus.OK})