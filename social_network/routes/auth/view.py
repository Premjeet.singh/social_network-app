from flask import jsonify,request, make_response
import uuid
import json
from http import HTTPStatus
from werkzeug.security import generate_password_hash, check_password_hash
from social_network import db
import jwt
from flask_admin.contrib.sqla import ModelView
import datetime
from social_network.models.socials.user import User
from social_network.models.connections.user_connection import UserConnectionMap
from social_network.models.likes_and_posts.likes import UserLikesAndShareMap
from social_network.models.socials.Post import Post
from social_network.models.socials.Post import Comment
from social_network.social_network_core.decorator import user_authentication
from flask import Blueprint, request, json
from flask.views import MethodView
from flask import current_app
from social_network.social_network_core.token import Authentication
auth_bp = Blueprint('auth', __name__)
authenticate = Authentication()


class UserRegister(MethodView):
    @user_authentication
    def post(self):
        post_data = request.get_json()
        user = User.query.filter_by(email=post_data.get('email')).first()
        hashed_password = generate_password_hash(post_data['password'], method='sha256')
        if not user:
            try:
                new_user = User(public_id=str(uuid.uuid4()), name=post_data['name'], password=hashed_password,
                                email=post_data['email'])
                user_data = {}
                user_data['public_id'] = new_user.public_id,
                user_data['email'] = new_user.email,
                user_data['name'] = new_user.name,
                user_data['password'] = new_user.password
                db.session.add(new_user)
                db.session.commit()
                return jsonify({'message': "registration successful !", "status": HTTPStatus.CREATED})
            except Exception as e:
                return jsonify({'message': "registration failure !", "status": HTTPStatus.UNAUTHORIZED})
        else:
            return jsonify({'message': "user exists with this email!", "status": HTTPStatus.FOUND})


class LoginAPI(MethodView):
    @user_authentication
    def post(self):
        auth = request.authorization
        if not auth or not auth.username or not auth.password:
            return jsonify({'message': "unable to login  !", "status": HTTPStatus.UNAUTHORIZED})

        user = User.query.filter_by(name=auth.username).first()

        if not user:
            return jsonify({'message': "user not found !", "status": HTTPStatus.NOT_FOUND})

        token = authenticate.authenticate_token(user, user.password, auth.password)
        return token


class UserListApi(MethodView):
    def get(self):
        # if not current_user.admin:
        #     return jsonify({'message' : 'Cannot perform that function!'})
        users = User.query.all()
        output = []
        for user in users:
            user_data = {}
            user_post=[]
            user_share_post=[]
            user_connection_post=[]
            user_comment=[]
            user_data['id']=user.id
            user_data['public_id'] = user.public_id
            user_data['name'] = user.name
            user_data['email'] = user.email
            user_data['password'] = user.password
            posts_data = Post.query.filter_by(author_id=user.id).all()
            if posts_data:
                for post in posts_data:
                    user_post_data = {}
                    user_post_data['post_id'] = post.id
                    user_post_data['post_content'] = post.content
                    user_post_data['user_id'] = post.author_id
                    user_post_data['like'] = post.like
                    user_post_data['flag'] = post.flag
                    user_post_data['share'] = post.share
                    user_post.append(user_post_data)
                user_data['posts']=user_post
            else:
                user_data['posts']=[]
            users = UserConnectionMap.query.filter(UserConnectionMap.user_id == user.id).all()
            user_connection_ids = [user.connection_id for user in users]
            posts = Post.query.filter(Post.author_id.in_(user_connection_ids)).all()
            posts_ids=[post.id for post in posts]
            user_post_likes_data = UserLikesAndShareMap.query.filter(UserLikesAndShareMap.post_id.in_(posts_ids),
                                                                UserLikesAndShareMap.user_id == user.id).filter(UserLikesAndShareMap.share==True).all()

            if user:
                for post in posts:
                    user_share_post_data = {}
                    user_share_post_data['post_id'] = post.id
                    user_share_post_data['post_content'] = post.content
                    user_share_post_data['user_id'] = post.author_id
                    user_share_post_data['like'] = post.like
                    user_share_post_data['flag'] = post.flag
                    user_share_post_data['share'] = post.share
                    user_connection_post.append(user_share_post_data)
                user_data['connection_posts'] = user_connection_post
            else:
                user_data['connection_posts'] = []
            posts_ids = [post.id for post in posts]
            user_post_likes_data = UserLikesAndShareMap.query.filter(UserLikesAndShareMap.post_id.in_(posts_ids),
                                                                     UserLikesAndShareMap.user_id == user.id).filter(
                UserLikesAndShareMap.share == True).all()
            share_post_ids = [post.post_id for post in user_post_likes_data]
            share_posts_data = Post.query.filter(Post.id.in_(share_post_ids)).all()
            if share_posts_data:
                for post in share_posts_data:
                    user_share_post_data = {}
                    user_share_post_data['post_id'] = post.id
                    user_share_post_data['post_content'] = post.content
                    user_share_post_data['user_id'] = post.author_id
                    user_share_post_data['like'] = post.like
                    user_share_post_data['flag'] = post.flag
                    user_share_post_data['share'] = post.share
                    user_share_post.append(user_share_post_data)
                user_data['shared_posts'] = user_share_post
            else:
                user_data['shared_posts'] = []
            comments_data = Comment.query.filter_by(author_id=user.id).all()
            if comments_data:
                for comment in comments_data:
                    user_comment_data = {}
                    user_comment_data['comment_id'] = comment.id
                    user_comment_data['content'] = comment.content
                    user_comment_data['user_id'] = comment.author_id
                    user_comment_data['post_id'] = comment.post_id
                    user_comment_data['date_posted'] = comment.date_posted
                    user_comment.append(user_comment_data)
                user_data['comments'] = user_comment
            else:
                user_data['comments'] = []
            output.append(user_data)
        return jsonify({"data":output, "status": HTTPStatus.OK})


class GetUserApi(MethodView):
    def get(self,user_id):
        # if not current_user.admin:
        #     return jsonify({'message' : 'Cannot perform that function!'})
        user = User.query.filter_by(id=user_id).first()
        if not user:
            return jsonify({'message': "user not found!", "status": HTTPStatus.NOT_FOUND})
        user_data = {}
        user_post=[]
        user_share_post=[]
        user_connection_post=[]
        user_comment=[]
        posts_data = Post.query.filter_by(author_id=user_id).all()
        if posts_data:
            for post in posts_data:
                user_post_data = {}
                user_post_data['post_id'] = post.id
                user_post_data['post_content'] = post.content
                user_post_data['user_id'] = post.author_id
                user_post_data['like'] = post.like
                user_post_data['flag'] = post.flag
                user_post_data['share'] = post.share
                user_post.append(user_post_data)
            user_data['posts'] = user_post
        else:
            user_data['posts'] = []
        users = UserConnectionMap.query.filter(UserConnectionMap.user_id == user.id).all()
        user_connection_ids = [user.connection_id for user in users]
        print(user_connection_ids)
        posts = Post.query.filter(Post.author_id.in_(user_connection_ids)).all()
        if posts:
            for post in posts:
                user_share_post_data = {}
                user_share_post_data['post_id'] = post.id
                user_share_post_data['post_content'] = post.content
                user_share_post_data['user_id'] = post.author_id
                user_share_post_data['like'] = post.like
                user_share_post_data['flag'] = post.flag
                user_share_post_data['share'] = post.share
                user_connection_post.append(user_share_post_data)
            user_data['connection_posts'] = user_connection_post
        else:
            user_data['connection_posts'] = []
        posts_ids = [post.id for post in posts]
        user_post_likes_data = UserLikesAndShareMap.query.filter(UserLikesAndShareMap.post_id.in_(posts_ids),
                                                                 UserLikesAndShareMap.user_id == user.id).filter(
            UserLikesAndShareMap.share == True).all()
        share_post_ids = [post.post_id for post in user_post_likes_data]
        share_posts_data = Post.query.filter(Post.id.in_(share_post_ids)).all()
        if share_posts_data:
            for post in share_posts_data:
                user_share_post_data = {}
                user_share_post_data['post_id'] = post.id
                user_share_post_data['post_content'] = post.content
                user_share_post_data['user_id'] = post.author_id
                user_share_post_data['like'] = post.like
                user_share_post_data['flag'] = post.flag
                user_share_post_data['share'] = post.share
                user_share_post.append(user_share_post_data)
            user_data['shared_posts'] = user_share_post
        else:
            user_data['shared_posts'] = []

        comments_data = Comment.query.filter_by(author_id=user.id).all()
        if comments_data:
            for comment in comments_data:
                user_comment_data = {}
                user_comment_data['d'] = comment.id
                user_comment_data['content'] = comment.content
                user_comment_data['user_id'] = comment.author_id
                user_comment_data['post_id'] = comment.post_id
                user_comment_data['date_posted'] = comment.date_posted
                user_comment.append(user_comment_data)
            user_data['comments'] = user_comment
        else:
            user_data['comments'] = []
        user_data['public_id'] = user.public_id
        user_data['email'] = user.email
        user_data['name'] = user.name
        user_data['password'] = user.password
        return jsonify({"data":user_data, "status": HTTPStatus.OK})


class GetFindUserApi(MethodView):
    def get(self):
        # if not current_user.admin:
        #     return jsonify({'message' : 'Cannot perform that function!'})
        # year_id = request.GET.get('name')
        params = request.args.to_dict()
        user_name = params.get("name", "")
        search = "%{}%".format(user_name)
        users = User.query.filter(User.name==user_name).all()
        if not users:
            return jsonify({'message': "user not found", "status": HTTPStatus.NOT_FOUND})
        output = []
        for user in users:
            user_data = {}
            user_post = []
            user_share_post = []
            user_connection_post=[]
            user_comment = []
            user_data['id'] = user.id
            user_data['public_id'] = user.public_id
            user_data['email'] = user.email
            user_data['name'] = user.name
            user_data['password'] = user.password
            posts_data = Post.query.filter_by(author_id=user.id).all()
            if posts_data:
                for post in posts_data:
                    user_post_data = {}
                    user_post_data['post_id'] = post.id
                    user_post_data['post_content'] = post.content
                    user_post_data['user_id'] = post.author_id
                    user_post_data['like'] = post.like
                    user_post_data['flag'] = post.flag
                    user_post_data['share'] = post.share
                    user_post.append(user_post_data)
                user_data['posts'] = user_post
            else:
                user_data['posts'] = []
            users = UserConnectionMap.query.filter(UserConnectionMap.user_id == user.id).all()
            user_connection_ids = [user.connection_id for user in users]
            posts = Post.query.filter(Post.author_id.in_(user_connection_ids)).all()
            if posts:
                for post in posts:
                    user_share_post_data = {}
                    user_share_post_data['post_id'] = post.id
                    user_share_post_data['post_content'] = post.content
                    user_share_post_data['user_id'] = post.author_id
                    user_share_post_data['like'] = post.like
                    user_share_post_data['flag'] = post.flag
                    user_share_post_data['share'] = post.share
                    user_connection_post.append(user_share_post_data)
                user_data['connection_posts'] = user_connection_post
            else:
                user_data['connection_posts'] = []
            posts_ids = [post.id for post in posts]
            user_post_likes_data = UserLikesAndShareMap.query.filter(UserLikesAndShareMap.post_id.in_(posts_ids),
                                                                     UserLikesAndShareMap.user_id == user.id).filter(
                UserLikesAndShareMap.share == True).all()
            share_post_ids = [post.post_id for post in user_post_likes_data]
            share_posts_data = Post.query.filter(Post.id.in_(share_post_ids)).all()
            if share_posts_data:
                for post in share_posts_data:
                    user_share_post_data = {}
                    user_share_post_data['post_id'] = post.id
                    user_share_post_data['post_content'] = post.content
                    user_share_post_data['user_id'] = post.author_id
                    user_share_post_data['like'] = post.like
                    user_share_post_data['flag'] = post.flag
                    user_share_post_data['share'] = post.share
                    user_share_post.append(user_share_post_data)
                user_data['shared_posts'] = user_share_post
            else:
                user_data['shared_posts'] = []
            comments_data = Comment.query.filter_by(author_id=user.id).all()
            if comments_data:
                for comment in comments_data:
                    user_comment_data = {}
                    user_comment_data['comment_id'] = comment.id
                    user_comment_data['content'] = comment.content
                    user_comment_data['user_id'] = comment.author_id
                    user_comment_data['post_id'] = comment.post_id
                    user_comment_data['date_posted'] = comment.date_posted
                    user_comment.append(user_comment_data)
                user_data['comments'] = user_comment
            else:
                user_data['comments'] = []
        output.append(user_data)
        return jsonify({"data":user_data, "status": HTTPStatus.FOUND})


class GetDeleteUserApi(MethodView):
    def delete(self,user_id):
        # if not current_user.admin:
        #     return jsonify({'message': 'only admin allowed to perform this function'})

        user =User.query.filter_by(id=user_id).first()
        if not user:
            return jsonify({'message': "unable to delete user !", "status": HTTPStatus.CONFLICT})
        posts = Post.query.filter(Post.author_id == user_id).all()
        posts_id = [post.id for post in posts]
        Comment.query.filter(Comment.post_id.in_(posts_id)).delete(synchronize_session="fetch")
        Post.query.filter(Post.author_id == user_id).delete(synchronize_session="fetch")
        User.query.filter(User.id == user_id).delete(synchronize_session="fetch")
        db.session.commit()
        return jsonify({'message': "user deleted!", "status": HTTPStatus.OK})


user_register_view = UserRegister.as_view('registration_api')
user_list_view = UserListApi.as_view('user_list_api')
get_user_view = GetUserApi.as_view('get_user_api')
get_find_user_view = GetFindUserApi.as_view('get_find_user_api')
get_delete_user_view = GetDeleteUserApi.as_view('get_delete__user_api')
login_view = LoginAPI.as_view('login_api')
# logout_view = LogoutAPI.as_view('logout_api')
auth_bp.add_url_rule(
    '/api/v1/auth/register',
    view_func=user_register_view,
    methods=['POST']
)
auth_bp.add_url_rule(
    '/api/v1/auth/login',
    view_func=login_view,
    methods=['POST']
)
auth_bp.add_url_rule(
    '/api/v1/auth/user_list',
    view_func=user_list_view,
    methods=['GET']
)
auth_bp.add_url_rule(
    '/api/v1/auth/user/<int:user_id>',
    view_func=get_user_view,
    methods=['GET']
)
auth_bp.add_url_rule(
    '/api/v1/auth/user/find',
    view_func=get_find_user_view,
    methods=['GET','DELETE']
)
auth_bp.add_url_rule(
    '/api/v1/auth/user/<int:user_id>',
    view_func=get_delete_user_view,
    methods=['DELETE']
)
# auth_bp.add_url_rule(
#     '/api/v1/auth/logout',
#     view_func=logout_view,
#     methods=['POST']
