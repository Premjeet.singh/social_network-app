from flask import jsonify,request, make_response
import uuid
import json
from http import HTTPStatus
from social_network import db
import jwt
from flask_admin.contrib.sqla import ModelView
import datetime
from social_network.models.connections.user_connection import UserConnectionMap
from social_network.social_network_core.decorator import user_authentication
from flask import Blueprint, request, json
from flask.views import MethodView
from flask import current_app
from social_network.services.posts_share_service import UserPostShareService
from social_network.social_network_core.token import Authentication
post_share_bp = Blueprint('api/v1/share', __name__)
authenticate = Authentication()


class PostShareApiView(MethodView):
    @user_authentication
    def post(self,user_id,post_id):
        post_data = request.get_json()
        services=UserPostShareService()
        post_data = request.get_json()
        ret = services.user_posts_share(post_data,user_id, post_id)
        return ret


post_share_view = PostShareApiView.as_view('get_likes_api')
post_share_bp.add_url_rule(
    '/api/v1/share/user/<int:user_id>/post/<int:post_id>',
    view_func=post_share_view,
    methods=['POST']
)