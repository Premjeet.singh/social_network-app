from flask import Blueprint, request, json
from flask.views import MethodView
from social_network.services.socials.post_service import UserPostService
from flask import current_app
import jwt
from social_network.models.socials.user import User
from social_network.social_network_core.decorator import user_authentication
user_post_bp = Blueprint('api/v1/user/<int:author_id>/post', __name__)


class UserPostAPIView(MethodView):
    def get(self, author_id):
        services = UserPostService()
        ret = services.get_posts(author_id)
        return ret

    @user_authentication
    def post(self,author_id):
        auth = request.headers.get('x-access-token')
        data = jwt.decode(auth, current_app.config['SECRET_KEY'])
        current_user = User.query.filter_by(public_id=data['public_id']).first()
        services = UserPostService()
        post_data = request.get_json()
        ret = services.create_post(author_id,post_data,current_user)
        return ret


class GetUserPostAPIView(MethodView):
    def get(self, author_id,post_id):
        services = UserPostService()
        ret = services.get_post(author_id,post_id)
        return ret

    @user_authentication
    def post(self,author_id,post_id):
        services = UserPostService()
        post_data = request.get_json()
        ret = services.update_post(author_id,post_id,post_data)
        return ret

    def delete(self,author_id,post_id):
        service = UserPostService()
        ret = service.delete_post(author_id,post_id)
        return ret


user_post_api_view = UserPostAPIView.as_view('user_post_api_view')
user_post_bp.add_url_rule(
    '/api/v1/user/<int:author_id>/post',
    view_func=user_post_api_view,
    methods=['GET', 'POST']
)

get_user_post_api_view = GetUserPostAPIView.as_view('get_user_post_api_view')
user_post_bp.add_url_rule(
    '/api/v1/user/<int:author_id>/post/<int:post_id>',
    view_func=get_user_post_api_view,
    methods=['GET', 'POST','DELETE']
)

