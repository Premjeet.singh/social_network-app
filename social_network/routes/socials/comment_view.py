from flask import Blueprint, request, json
from flask.views import MethodView
from http import HTTPStatus
from flask import current_app
import jwt
from social_network.models.socials.user import User
from social_network.services.socials.comment_service import UserCommentService
from social_network.social_network_core.decorator import user_authentication

user_comment_bp = Blueprint('api/v1/user/<int:author_id>/post/<int:post_id>/comment', __name__)


class UserCommentAPIView(MethodView):
    def get(self, author_id,post_id):
        services = UserCommentService()
        ret = services.get_comments(author_id,post_id)
        return ret

    @user_authentication
    def post(self,author_id,post_id):
        auth = request.headers.get('x-access-token')
        data = jwt.decode(auth, current_app.config['SECRET_KEY'])
        current_user = User.query.filter_by(public_id=data['public_id']).first()
        services = UserCommentService()
        post_data = request.get_json()
        ret = services.create_comment(author_id,post_id,post_data,current_user)
        return ret


class GetUserCommentAPIView(MethodView):
    def get(self, author_id,post_id,comment_id):
        services = UserCommentService()
        ret = services.get_comment(author_id,post_id,comment_id)
        return ret

    @user_authentication
    def post(self,author_id,post_id,comment_id):
        services = UserCommentService()
        post_data = request.get_json()
        ret = services.update_comment(author_id,post_id,comment_id,post_data)
        return ret

    def delete(self,author_id,post_id, comment_id):
        service = UserCommentService()
        ret = service.delete_comment(author_id,post_id,comment_id)
        return ret


user_comment_api_view = UserCommentAPIView.as_view('user_comment_api_view')
user_comment_bp.add_url_rule(
    '/api/v1/user/<int:author_id>/post/<int:post_id>/comment',
    view_func=user_comment_api_view,
    methods=['GET', 'POST']
)

get_user_comment_api_view = GetUserCommentAPIView.as_view('get_user_comment_api_view')
user_comment_bp.add_url_rule(
    '/api/v1/user/<int:author_id>/post/<int:post_id>/comment/<int:comment_id>',
    view_func=get_user_comment_api_view,
    methods=['GET', 'POST','DELETE']
)

