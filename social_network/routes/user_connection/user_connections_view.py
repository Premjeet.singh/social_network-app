from flask import jsonify,request, make_response
import uuid
import json
from http import HTTPStatus
from social_network import db
import jwt
from flask_admin.contrib.sqla import ModelView
from social_network.models.socials.user import User
from social_network.social_network_core.decorator import user_authentication
import datetime
from social_network.models.connections.user_connection import UserConnectionMap
from flask import Blueprint, request, json
from flask.views import MethodView
from flask import current_app
from social_network.social_network_core.token import Authentication
user_connection_bp = Blueprint('api/v1/user/<int:author_id>/assign_connection', __name__)
authenticate = Authentication()


class AddingConnectionToUserApi(MethodView):
    @user_authentication
    def post(self, user_id):
        post_data = request.get_json()
        users = UserConnectionMap.query.filter(
            UserConnectionMap.user_id == user_id).all()
        existing_users = [user.connection_id for user in users]
        new_users = post_data.get("connections", [])
        connection_to_be_deleted = list(set(existing_users) - set(new_users))
        connection_to_be_created = list(set(new_users) - set(existing_users))
        try:
            connection_to_be_created=User.query.filter(User.id.in_(connection_to_be_created)).all()
            if connection_to_be_created:
                for connection_id in connection_to_be_created:
                    obj = UserConnectionMap(user_id=user_id, connection_id=connection_id.id)
                    db.session.add(obj)
            else:
                return jsonify({'message': "unable to create connection users not exists or connection is already created!", "status": HTTPStatus.NOT_FOUND})
            db.session.commit()
            return jsonify({'message': "connection created succesfully!", "status": HTTPStatus.CREATED})
        except Exception as e:
            return jsonify({'message': "unable to create connection!", "status": HTTPStatus.CONFLICT})


get_connection_view = AddingConnectionToUserApi.as_view('get_connection_api')
user_connection_bp.add_url_rule(
    '/api/v1/user/<int:user_id>/assign_connection',
    view_func=get_connection_view,
    methods=['POST']
)