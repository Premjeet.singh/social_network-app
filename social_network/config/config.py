import os

SQL_USER = 'root'
SQL_PASSWORD = 'root'
SQL_DATABASE = 'social_networking_db'


class Config(object):
    SQLALCHEMY_ECHO =  True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    TESTING = True


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        'mysql+pymysql://{nam}:{pas}@127.0.0.1:3306/{dbn}').format(
        nam=SQL_USER,
        pas=SQL_PASSWORD,
        dbn=SQL_DATABASE,
    )


app_config = {
    "development": DevelopmentConfig,
}
