from social_network import db
from flask_admin.contrib.sqla import ModelView


class UserLikesAndShareMap(db.Model):
    __tablename__ = "user_likes_share"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), nullable=False)
    post_id =db.Column(db.Integer(),nullable=False)
    like=db.Column(db.Boolean(),default=False)
    share=db.Column(db.Boolean(),default=False)
