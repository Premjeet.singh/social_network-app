from social_network import db
from flask_admin.contrib.sqla import ModelView


class UserConnectionMap(db.Model):
    __tablename__ = "user_connection"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), nullable=False)
    connection_id = db.Column(db.Integer(), nullable=False)



