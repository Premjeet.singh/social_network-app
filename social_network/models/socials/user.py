from social_network import db
from flask_admin.contrib.sqla import ModelView
from social_network.models.socials.Comment import Comment
from social_network.models.socials.Post import Post

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(20))
    name = db.Column(db.String(20))
    password = db.Column(db.String(100))
    posts = db.relationship('Post', backref='users', lazy=True)
    comments = db.relationship('Comment', backref='users', lazy=True)


    def __repr__(self):
        return '<User %r>' % (self.name)




