from social_network import db
from datetime import datetime
from social_network.models.socials.Comment import Comment
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user


class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    content = db.Column(db.Text, nullable=False)
    like=db.Column(db.Boolean(),default=False)
    share=db.Column(db.Boolean(),default=False)
    flag=db.Column(db.Boolean(),default=False)
    comments = db.relationship('Comment', backref='posts', lazy=True)

    def __repr__(self):
        return "Post(Title:'{}', Content:'{}')".format(self.id, self.content)


