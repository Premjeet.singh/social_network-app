from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_migrate import Migrate
from social_network.config.config import app_config

db = SQLAlchemy()


def create_app(test_config=None):
    app = Flask(__name__)
    app.config.from_object(app_config["development"])
    app.config['SECRET_KEY'] = 'mysecret'
    db.init_app(app)
    Migrate(app, db)
    admin = Admin(app)
    from social_network.models.socials.user import User
    from social_network.models.socials.Comment import Comment
    from social_network.models.socials.Post import Post
    from social_network.models.likes_and_posts.likes import UserLikesAndShareMap
    with app.app_context():
        from social_network.routes.socials.post_view import user_post_bp
        app.register_blueprint(user_post_bp)
        from social_network.routes.socials.comment_view import user_comment_bp
        app.register_blueprint(user_comment_bp)
        from social_network.routes.auth.view import auth_bp
        app.register_blueprint(auth_bp)
        from social_network.routes.user_connection.user_connections_view import user_connection_bp
        app.register_blueprint(user_connection_bp)
        from social_network.routes.posts_like_view import post_likes_bp
        app.register_blueprint(post_likes_bp)
        from social_network.routes.posts_share_view import post_share_bp
        app.register_blueprint(post_share_bp)
    #
    # admin.add_view(ModelView(User, db.session))
    # admin.add_view(ModelView(ProductName, db.session))
    # admin.add_view(CategoryView(Category, db.session))
    return app




